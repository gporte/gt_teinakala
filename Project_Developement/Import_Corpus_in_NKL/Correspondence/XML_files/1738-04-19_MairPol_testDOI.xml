<?xml version="1.0" encoding="UTF-8"?>


<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="MAIR_POL_1738_04_19">

    <teiHeader>
        <!-- Métadonnées de la lettre en question. -->
        <fileDesc>
            <titleStmt>
                <!-- Mettre l'entrée de la lettre dans l'index comme elle apparaîtra sur le site. -->
                <title>Lettre de Jean-Jacques Dortous de Mairan à Giovanni Poleni, Paris, 19 avril
                    1738.</title>
                <!-- Chercheurs qui ont travaillé sur la lettre. Qui a fait quoi. Associer un identifiant à la personne pour pouvoir faire les liens vers les notes et commentaires dont elle est l'auteur. -->
                <respStmt>
                    <resp>Encodage</resp>
                    <persName corresp="CH">Cécile Hamon</persName>
                </respStmt>
                <respStmt>
                    <resp>Vérification encodage</resp>
                    <persName corresp="GP">Gwenaëlle Patat</persName>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <!-- Pas besoin de répéter les indications mises dans les métadonnées du projet, on peut se contenter de ce qui est indiqué si-dessous. -->
                <publisher/>
                <idno xml:base="URI absolue pointant vers donnée" type="DOI">identifiant de la donnée dans Nakala</idno>
            </publicationStmt>
            <!-- Informations relatives à la source qui sert de base à l'édition électronique, manuscrite ou numérisée. -->
            <sourceDesc>
                <!-- Pour une source manuscrite, rappeler sa cote et où elle est conservée. -->
                <msDesc>
                    <msIdentifier>
                        <country>Italie</country>
                        <settlement>Vérone</settlement>
                        <repository>Biblioteca civica</repository>
                        <idno>3096 E, f°377</idno>
                    </msIdentifier>
                    <physDesc>
                        <!-- Draft, copy or letter? -->
                        <objectDesc form="draft">
                            <supportDesc material="paper">
                                <support>Papier</support>
                            </supportDesc>
                        </objectDesc>
                        <!-- Indiquer la couleur de l'encre. Se mettre d'accord sur une typologie pour l'@medium : brown-ink, red-ink, etc. -->
                        <handDesc>
                            <handNote medium="brown-ink">De la main de Jean-Jacques Dortous de
                                Mairan.</handNote>
                        </handDesc>
                    </physDesc>
                    <additional>
                        <!-- Mettre ici les références bibliographiques liées à la lettre en question. -->
                        <surrogates>
                            <!-- On peut choisir un <bibl> ou bien un <biblStruct>, format davantage structuré, qui est généré automatiquement depuis l'export TEI de Zotero. -->
                            <p>
                                <bibl/>
                            </p>
                        </surrogates>
                    </additional>
                </msDesc>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <correspDesc>
                <!-- Dans <correspAction>, mettres les infos d'envoie et de réception. Ne pas mettre le lieu et la date si on les connaît pas.-->
                <correspAction type="sent">
                    <persName corresp="#Mairan">DORTOUS DE MAIRAN, Jean-Jacques</persName>
                    <settlement>Paris</settlement>
                    <date when="1738-04-19">19 avril 1738</date>
                </correspAction>
                <correspAction type="received">
                    <persName corresp="#Poleni">POLENI, Giovanni</persName>
                    <!-- On peut supprimer ces balises si on ne connaît pas les informations pour les remplir. -->
                    <settlement/>
                    <date/>
                </correspAction>
                <!-- Dans <correspContext>, mettre les flux dans laquelle la lettre s'inscrit : à quelle lettre elle répond ? Quelle lettre la suit ? On peut faire un @corresp qui pointe vers la lettre précédente ou suivante en se référant à leur xml:id. Rester prudent sur les lettres précédentes et suivantes (cf. cas des lettres intermédiaires). -->
                <!--                <correspContext>
                    <ref type="prev"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        précédente de <persName corresp="#Poleni">??</persName> à <persName
                            corresp="#Hermann">??</persName>: <date when="1731">??</date>
                    </ref>
                    <ref type="next"
                        corresp="#date3PremieresLettresEmettereur3PremieresLettresRecepteur">Lettre
                        suivante de <persName corresp="#Hermann">??</persName> à <persName
                            corresp="#Poleni">??</persName>: <date when="1731-11">??</date>
                    </ref>
                </correspContext>-->
            </correspDesc>
            <!-- Résumé du contenu apparaissant dans la notice.-->
            <abstract>
                <p/>
            </abstract>
            <textClass>
                <!-- Liste de balises <term> et renvoie vers des termes indexés si besoin.-->
                <keywords>
                    <term/>
                </keywords>
            </textClass>
        </profileDesc>
        <revisionDesc>
            <change when="2021-04-09" who="CH">Proposition de l'encodage</change>
            <change when="2021-04-09" who="GP">Première relecture de l'encodage</change>
            <change when="2021-04-12" who="CH">Correction de l'encodage</change>
            <change when="2021-04-16" who="CH">Relève des noms d'ouvrages et inscriptions dans
                l'index du teicorpus, balisage des titres en italique avec
                    <att>rend</att><val>italic</val> et des mots en italique ou soulignés avec la
                balise <gi>hi</gi> et les attributs <att>rend</att><val>italic</val> et
                    <att>rend</att><val>underline</val>.</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-21" who="GP">Relecture et correction des modifications de
                l'encodage</change>
            <change when="2021-04-27" who="CH">Dernières modifications de l'encodage</change>
            <change when="2021-05-03" who="GP">Relecture des dernières modifications de
                l'encodage</change>
        </revisionDesc>
    </teiHeader>

    <!-- Corps de la lettre. -->
    <text>
        <body>
            <div type="transcription" xml:lang="fr">
                <pb xml:id="MairPol_1_1738-04-19" n="Transcription_1738-04-19_MairPol_1"
                    facs="data/images/single/1738-04-19_MairPol/1738-04-19_MairPol_1.jpg"
                    type="recto"/>
                <opener>
                    <salute><rs type="person" corresp="#Poleni">Monsieur,</rs></salute>
                </opener>
                <p>Peu de temps après que je me fus donné l’honneur de vous écrire dans le mois
                    dernier, on nous apporta à <rs type="org" corresp="#AcadRSciencesFr"
                        >l’Académie</rs> les pièces imprimées du prix de 1737. J’étais sur le point
                    de partir pour la campagne, d’où je ne suis revenu que le 16 de ce mois, et
                        <persName corresp="#Morand">Mr Morand</persName> qui est en commerce avec
                    des personnes de votre pays, ayant bien voulu se charger de vous faire tenir les
                    exemplaires qui vous étaient destinés, je les lui remis, au nombre de 6 de votre
                    ouvrage seulement, avec un exemplaire entier des 4 pièces. J’ai fait mon
                    possible pour en obtenir davantage ; mais on n’a pas voulu s’éloigner de la
                    règle établie là-dessus.</p>
                <closer>Je vous renouvelle les offres de mes services, et les assurances du respect
                    avec lequel j’ai l’honneur d’être <lb/>
                    <rs type="person" corresp="#Poleni">Monsieur</rs><lb/>
                    <signed><lb/><rs type="person" corresp="#Mairan">Votre très humble et très
                            obéissant serviteur,</rs>
                        <persName corresp="#Mairan">Dortous de Mairan.</persName></signed>
                </closer>
            </div>
        </body>
    </text>

</TEI>

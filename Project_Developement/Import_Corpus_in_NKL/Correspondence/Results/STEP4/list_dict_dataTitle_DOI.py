list_dict_dataTitle_DOI = [{'CNRS-CFEETK 196637. Karnak, KIU 5807 / Magasin Nord 6 (MN6), Scene, mur nord 4.o : offrande de la laitue XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.0c727352'}
{'CNRS-CFEETK 196638. Karnak, KIU 5807 / Magasin Nord 6 (MN6), Scene, mur nord 4.o : offrande de la laitue XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.bf33exo2'}
{'img_orn_1': '10.34847/nkl.47e4xg1m'}
{'CNRS-CFEETK 188007. Karnak, KIU 720 / support au nom de sobekhotep iii XIIIe dynastie / Sobekhotep III': '10.34847/nkl.739dm7ss'}
{'CNRS-CFEETK 55101. Karnak,': '10.34847/nkl.21559bfh'}
{'CNRS-CFEETK 23560. Karnak,': '10.34847/nkl.184bc10z'}
{'CNRS-CFEETK 27535. Karnak,': '10.34847/nkl.18d4748m'}
{'CNRS-CFEETK 31992. Karnak,': '10.34847/nkl.f518icj3'}
{'CNRS-CFEETK 31993. Karnak, KIU 3399 / Magasin nord 7 (MN7), Scene, mur nord 5.o : accomplir la libation XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.e28e0j17'}
{'CNRS-CFEETK 31994. Karnak, KIU 3399 / Magasin nord 7 (MN7), Scene, mur nord 5.o : accomplir la libation XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.a9f9j9gl'}
{'CNRS-CFEETK 31995. Karnak, KIU 3399 / Magasin nord 7 (MN7), Scene, mur nord 5.o : accomplir la libation XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.bdc83918'}
{'CNRS-CFEETK 31996. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.de3de990'}
{'CNRS-CFEETK 31997. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.aab7eml0'}
{'CNRS-CFEETK 31999. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.e8bb5yw4'}
{'CNRS-CFEETK 32000. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.0cd047zd'}
{'CNRS-CFEETK 48899. Karnak, KIU 3399 / Magasin nord 7 (MN7), Scene, mur nord 5.o : accomplir la libation XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.22a4nf7p'}
{'CNRS-CFEETK 48900. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.d899617g'}
{'CNRS-CFEETK 48901. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.0fc8m9za'}
{'CNRS-CFEETK 48902. Karnak, KIU 3396 / Magasin nord 7 (MN7), Scene, mur nord 2.o : accomplir l’encensement XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.0527vve7'}
{'CNRS-CFEETK 50056. Karnak, KIU 1425 / Chapelle d’Osiris Heqa-djet, Scene, salle 1, paroi nord, 1er registre 02.o : jouer des sistres XXVe dynastie / Chabataka': '10.34847/nkl.5bcd65qv'}
{'CNRS-CFEETK 50057. Karnak, KIU 1425 / Chapelle d’Osiris Heqa-djet, Scene, salle 1, paroi nord, 1er registre 02.o : jouer des sistres XXVe dynastie / Chabataka': '10.34847/nkl.d13dei8w'}
{'CNRS-CFEETK 53725. Karnak, KIU 3398 / Magasin nord 7 (MN7), Scene, mur nord 4.o : course royale XVIIIe dynastie / Thoutmosis III': '10.34847/nkl.9dee8in4'}
{'CNRS-CFEETK 55100. Karnak, KIU 1404 / Chapelle d’Osiris Heqa-djet, Scene, salle 1, paroi est, 2e registre 02.n : don de la vie XXVe dynastie / Chabataka': '10.34847/nkl.3d65kz9u'}
{'CE1.1 Journal du désert, p.1, 6 mars': '10.34847/nkl.0941l9t0'}]